# TODO Change to  env variables

#Load Zones
curl https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/zones.csv -o zones.csv
mysql -uroot -ppassword -h127.0.0.1 sketch-challenge --local_infile <<EOF
LOAD DATA LOCAL INFILE 'zones.csv'
REPLACE INTO TABLE zones
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(id, borough, name, service_zone);
EOF
rm zones.csv


## Green taxis
curl https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/green_tripdata_2018-01_01-15.csv -o green_taxis.csv
mysql -uroot -ppassword -h127.0.0.1 sketch-challenge --local_infile <<EOF
LOAD DATA LOCAL INFILE 'green_taxis.csv'
INTO TABLE trips
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r\n'
(@dummy,pickup_datetime , dropoff_datetime, @dummy, @dummy, pickup_location_id, dropoff_location_id);
EOF
rm green_taxis.csv


## yellow taxis
curl https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/yellow_tripdata_2018-01_01-15.csv -o yellow_taxis.csv
mysql -uroot -ppassword -h127.0.0.1 sketch-challenge --local_infile <<EOF
LOAD DATA LOCAL INFILE 'yellow_taxis.csv'
INTO TABLE trips
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r\n'
(@dummy,pickup_datetime , dropoff_datetime, @dummy, @dummy,@dummy, @dummy, pickup_location_id, dropoff_location_id);
EOF
rm yellow_taxis.csv

# Calculate top trips
mysql -uroot -ppassword -h127.0.0.1 sketch-challenge --local_infile <<EOF
INSERT INTO trips_by_zone (id, name, pu_total, do_total)
SELECT
zones.id as id,
zones.name as zone,
SUM(trips.pickup_location_id = zones.id) as 'pu_total',
SUM(trips.dropoff_location_id = zones.id) as 'do_total'
FROM  trips
JOIN zones ON trips.pickup_location_id = zones.id OR trips.dropoff_location_id = zones.id
GROUP BY zones.id
EOF