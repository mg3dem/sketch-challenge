

# Full-stack Sketch's Technical Challenge (Internal Tools)

Test project for Sketch's Full-stack position (Internal tools).
It is a simple tool that allows you to have access to data about passenger pickups and drop-offs in your city.

The project has 3 main independent parts:

### cron job to fetch and load data
Small bash script intended to run as a cron job to fetch and load data, and calculate intense processing information.
As an example, we will use some data provided by NYC regarding taxi trips during certain days of January 2018. This data can be downloaded from:

 - Yellow taxis - [https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/yellow_tripdata%20_2018-01_01-15.csv](https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/yellow_tripdata%20_2018-01_01-15.csv)
- Green taxis - [https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/green_tripdata_2018-01_01-15.csv](https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/green_tripdata_2018-01_01-15.csv)
- Zones list - [https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/zones.csv](https://s3.amazonaws.com/infrastructure.sketch.cloud/fullstack-code-test/zones.csv)

### API server

#### http://localhost:8000

NodeJS Express app exposing a REST API to query the database. 3 endpoints are developed and accessible as GET and POST request:


##### http://localhost:8000/top-zones/?order=<pickups|dropoffs>

Will return a list of the 5 first zones order by the number of total pickups or dropoffs. This endpoint will accept 1 parameter:

* **order**: (can be "dropoffs" or "pickups")
  


##### http://localhost:8000/zone-trips/?zone=<zone-id>&date=<yyyy-mm-dd>

Will return the sum of the pickups and dropoffs in one zone and one date. This endpoint will accept 2 parameters:
  
  * **zone**: value must be the zone id of any of the available zones
  
  * **date**: value must be a date
  
  

##### http://localhost:8000/zones

Will return the list of available zones.

### web client.

#### http://localhost:3000

Small one-page ReactJS application to query the API server and display the information in 2 simple tables

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. This project does not include production deployment instructions or infrastructure.

**These instructions are intended to be executed on an Apple Mac system, for Windows or Linux steps may vary.**


### Prerequisites

To run this project properly, you will need Docker and MySQL.

#### Docker
Download and install [Docker for Mac](https://download.docker.com/mac/stable/Docker.dmg)

#### MySQL Client
Best option to install MySQL Client on a mac is using Homebrew package manager:

```brew install mysql-client```

If you need to install [Homebrew](https://brew.sh):

```/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```

If you do not want to use homebrew, you can get more information at [https://dev.mysql.com/downloads/shell/](https://dev.mysql.com/downloads/shell/)

### Installing

Clone this repository:

```git clone https://mg3dem@bitbucket.org/mg3dem/sketch-challenge.git```

In the project folder, run the following commands in a terminal to start the dockers:

```docker-compose build```

```docker-compose up -d```


If it is the first time you run the project, the database will be empty: to load the taxi trips and zones information, run the following command:

```./cronjob/loaddata.sh```

After these steps, you will get a fully working API server connected with a MySQL database containing the trips and zones information and a fully working client app running at http://localhost:3000

![Client screenshot](/client/public/client.png)

## Running the tests

Unit tests are not included in this version because of the time limit

## TODO


[ ] Unit testing

[ ] Better input validation in the server

[ ] Dockerize cronjob to fetch and load the db

[ ] Extra ball

## Author

* **Miguel Gallardo**  - [mg3dem](https://bitbucket.com/mg3dem)
