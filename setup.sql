DROP TABLE IF EXISTS `sketch-challenge`.`trips` ;
CREATE TABLE `sketch-challenge`.`trips` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pickup_datetime` datetime DEFAULT NULL,
  `dropoff_datetime` datetime DEFAULT NULL,
  `pickup_location_id` smallint(6) unsigned DEFAULT NULL,
  `dropoff_location_id` smallint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pickup_location_id` (`pickup_location_id`),
  KEY `dropoff_location_id` (`dropoff_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sketch-challenge`.`zones` ;
CREATE TABLE `sketch-challenge`.`zones` (
  `id` smallint(6) unsigned NOT NULL,
  `borough` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `service_zone` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sketch-challenge`.`trips_by_zone` ;
CREATE TABLE `sketch-challenge`.`trips_by_zone` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `pu_total` int(11) DEFAULT NULL,
  `do_total` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `sketch-challenge`.`trips`
ADD CONSTRAINT `trips_ibfk_1`
FOREIGN KEY (`pickup_location_id`) REFERENCES `zones` (`id`);

ALTER TABLE `sketch-challenge`.`trips`
ADD CONSTRAINT `trips_ibfk_2`
FOREIGN KEY (`dropoff_location_id`) REFERENCES `zones` (`id`);