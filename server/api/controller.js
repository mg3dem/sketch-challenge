const mysqlLib = require('../lib/mysql');

const { connection } = mysqlLib;

exports.getTopZones = function (req, res) {
  const params = req.method === 'POST' ? req.body : req.query;
  let order = 'pu_total';

  if (params.order === undefined || params.order === '') {
    res.status(422).send('Invalid or missing parameter');
    return;
  }

  if (params.order === 'dropoffs') {
    order = 'do_total';
  }

  connection.query(
    `SELECT id, name, pu_total, do_total FROM trips_by_zone ORDER BY ${order} DESC LIMIT 5`,
    function (error, results) {
      if (results) {
        res.json(results);
      } else {
        res.status(500).send(error);
      }
    },
  );
};

exports.getZoneTrips = function (req, res) {
  const params = req.method === 'POST' ? req.body : req.query;
  // TODO add parameters validation of type and format
  if (params.zone === undefined
    || params.zone === ''
    || params.date === undefined
    || params.date === '') {
    res.status(422).send('Invalid or missing parameter');
    return;
  }

  connection.query(
    `SELECT zones.name,
    DATE_FORMAT(DATE('${params.date}'),'%Y-%m-%d') as date,
    SUM( trips.pickup_location_id =zones.id ) as 'pu_total',
    SUM( trips.dropoff_location_id = zones.id ) as 'do_total'
    FROM  trips
    JOIN zones ON trips.pickup_location_id = zones.id OR trips.dropoff_location_id = zones.id
    WHERE (DATE(trips.dropoff_datetime) = DATE(${connection.escape(params.date)}) or
    DATE(trips.pickup_datetime) = DATE(${connection.escape(params.date)})) and
    zones.id = ${connection.escape(params.zone)}
    GROUP BY zones.id`,
    function (error, results) {
      if (results) {
        res.json(results);
      } else {
        res.status(500).send(error);
      }
    },
  );
};

exports.getZones = function (req, res) {

  connection.query(
    'SELECT id, borough, name, service_zone FROM zones ORDER BY name ASC',
    function (error, results) {
      if (results) {
        res.json(results);
      } else {
        res.status(500).send(error);
      }
    },
  );
};
