const controller = require('./controller');

module.exports = function (app) {
  app
    .route('/top-zones')
    .get(controller.getTopZones)
    .post(controller.getTopZones);

  app
    .route('/zone-trips')
    .get(controller.getZoneTrips)
    .post(controller.getZoneTrips);
  app
    .route('/zones')
    .get(controller.getZones)
    .post(controller.getZones);
};
