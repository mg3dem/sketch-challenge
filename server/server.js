const express = require('express');
const cors = require('cors');
const routes = require('./api/routes');

const app = express();
const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

routes(app);

app.listen(port);

console.log(`Sketch challenge server started on: ${port}`);
