const mysql = require('mysql');
const mysqlConfig = {
  host: process.env.MYSQL_HOST_IP,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE
}


var connection = mysql.createConnection(mysqlConfig);
connection.connect(function (err) {
  if (err) {
    console.log(`error connecting: ${err.stack}`);
  }
  console.log('connected to MySQL');
});

module.exports = {
  connection: mysql.createConnection(mysqlConfig)
}