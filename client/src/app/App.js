import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { format } from 'date-fns';
import { Row, Col, Container, Card, Form, Table, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import Select from 'react-select'

function App() {
  const [zoneTripsDate, setZoneTripsDate] = useState(null);
  const [zoneOptions, setZoneOptions] = useState(null);
  const [order, setOrder] = useState(null);
  const [topZones, setTopZones] = useState(false);
  const [isTopZonesLoading, setIsTopZonesLoading] = useState(false);
  const [zone, setZone] = useState(null);
  const [zoneTrips, setZoneTrips] = useState({});
  const [isZoneTripsLoading, setIsZoneTripsLoading] = useState(false);

  useEffect(() => {
    async function fetchZones() {
      const result = await axios(
        '//localhost:8000/zones',
      );
      const options = [];
      result.data.map(zone => {
        options.push({ value: zone.id, label: zone.name });
      });
      setZoneOptions(options);
    }
    fetchZones();
    //trigger top zones load
    setOrder('pickups');

  }, []);

  useEffect(() => {
    async function fetchZoneTrips(zone, zoneTripsDate) {
      setIsZoneTripsLoading(true);
      const result = await axios(
        `http://localhost:8000/zone-trips/?zone=${zone}&date=${zoneTripsDate}`
      );
      setZoneTrips(result.data[0]);
      setIsZoneTripsLoading(false);
    }

    if (zone !== null && zoneTripsDate !== null) {
      fetchZoneTrips(zone.value, format(zoneTripsDate, 'yyyy-MM-dd'));
    }
  }, [zone, zoneTripsDate]);

  useEffect(() => {
    async function fetchTopZones(order) {
      setIsTopZonesLoading(true);
      console.log(order);
      const result = await axios(
        `http://localhost:8000/top-zones/?order=${order}`
      );
      console.log(result.data);
      setTopZones(result.data);
      setIsTopZonesLoading(false);
    }

    if (order !== null) {
      fetchTopZones(order);
    }
  }, [order]);

  return (
    <Container fluid>
      <Row>
        <Col className="text-center mt-5 mb-5"><h1>Taxi trips</h1></Col>
      </Row>
      <Row>
        <Col>
          <Card className="mx-auto w-50">
            <Card.Body>
              <Card.Title>Top zones</Card.Title>
              <Form>
                <Form.Group as={Row} controlId="formHorizontalEmail">
                  <Form.Label column sm={2} className="text-right">
                    <h6>Order</h6>
                  </Form.Label>
                  <Col sm={3}>
                    <Form.Control as="select" custom onChange={e => setOrder(e.target.value)} >
                      <option value="pickups">Pick up</option>
                      <option value="dropoffs">Drop off</option>
                    </Form.Control>
                  </Col>
                </Form.Group>
              </Form>
              {isTopZonesLoading ? (
                <div className="mx-auto text-center" style={{ height: "201.5625px" }}>
                  <Spinner animation="border" className="align-middle" style={{ width: "3rem", height: "3rem" }} />
                </div>
              ) : (
                  <>{
                    topZones ? (
                      <>
                        <h5>Top {order === 'pickups' ? 'Pick-ups' : 'Drop-offs'}</h5>
                        <Table borderless className="w-75" size="sm">
                          <thead>
                            <tr>
                              <th className="">Zone</th>
                              <th className="">Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            {topZones.map(zone => (
                              <tr key={zone.name}>
                                <td>{zone.name}</td>
                                <td>{order === 'pickups' ? zone.pu_total : zone.do_total}</td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      </>
                    ) : ''}
                  </>
                )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card className="mx-auto w-50 mt-5">
            <Card.Body>
              <Card.Title>Zone trips</Card.Title>
              <Form>
                <Form.Group as={Row} controlId="formHorizontalEmail">
                  <Form.Label column sm={2} className="text-right">
                    <h6>Zone</h6>
                  </Form.Label>
                  <Col sm={4}>
                    <Select
                      options={zoneOptions}
                      value={zone}
                      onChange={selectedOption => setZone(selectedOption)}
                    />
                  </Col>
                  <Form.Label column sm={2} className="text-right">
                    <h6>Date</h6>
                  </Form.Label>
                  <Col sm={3}>
                    <DatePicker
                      selected={zoneTripsDate}
                      onChange={date => setZoneTripsDate(date)}
                      minDate={new Date(2018, 0, 1)}
                      maxDate={new Date(2018, 0, 31)}
                      placeholderText="Select a date"
                      className="form-control"
                    />
                  </Col>
                </Form.Group>
              </Form>
              <Row>
                <Col sm={2}><strong>Zone</strong></Col>
                <Col>{(zone && zone.label) ? zone.label : ''}</Col>
              </Row>
              <Row>
                <Col sm={2}><strong>Date</strong></Col>
                <Col>{(zoneTripsDate) ? format(zoneTripsDate, 'MM/dd/yyyy') : ''}</Col>
              </Row>
              <Row>
                <Col sm={2}><strong>Pickups</strong></Col>
                <Col>
                  {isZoneTripsLoading ? (
                    <Spinner animation="border" size="sm" />
                  ) : (zoneTrips.pu_total)}
                </Col>
              </Row>
              <Row>
                <Col sm={2}><strong>Dropoffs</strong></Col>

                <Col>
                  {isZoneTripsLoading ? (
                    <Spinner animation="border" size="sm" />
                  ) : (zoneTrips.do_total)}
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>

    </Container >
  );
}

export default App;